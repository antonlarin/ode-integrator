#-------------------------------------------------
#
# Project created by QtCreator 2015-04-07T06:12:27
#
#-------------------------------------------------

QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = odeintegrator
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11 -Wall -Wextra -pedantic

SOURCES += main.cpp\
    gui/odesmainwindow.cpp \
    ext/qcustomplot.cpp \
    ode/odeintegrator.tpp \
    ode/concreterhsfunctions.cpp \
    gui/observer.cpp \
    gui/odesmodel.cpp \
    gui/odescontroller.cpp \
    gui/odesprogressdialog.cpp \
    gui/odesresultsdialog.cpp \
    gui/odestestproblemtablemodel.cpp \
    gui/odesmainproblem1tablemodel.cpp \
    gui/odesmainproblem2tablemodel.cpp

HEADERS  += gui/odesmainwindow.hpp \
    ext/qcustomplot.h \
    ode/lecontroltype.hpp \
    ode/trajectorypoint.hpp \
    ode/cauchyproblem.hpp \
    ode/rhsfunction.hpp \
    ode/integrationmethod.hpp \
    ode/rungekutta4method.hpp \
    ode/iterationinfo.hpp \
    ode/odeintegrator.hpp \
    ode/concreterhsfunctions.hpp \
    gui/observer.hpp \
    gui/odesmodel.hpp \
    gui/odescontroller.hpp \
    gui/odesprogressdialog.hpp \
    gui/odesresultsdialog.hpp \
    gui/odestestproblemtablemodel.hpp \
    gui/guiutilities.hpp \
    gui/odesmainproblem1tablemodel.hpp \
    gui/odesmainproblem2tablemodel.hpp

FORMS    += gui/odesmainwindow.ui \
    gui/odesprogressdialog.ui \
    gui/odesresultsdialog.ui

RESOURCES += \
    gui/images.qrc

