#include <QApplication>

#include "gui/odesmodel.hpp"
#include "gui/odescontroller.hpp"
#include "gui/odesmainwindow.hpp"
#include "gui/odesprogressdialog.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ODESModel m;
    ODESMainWindow w(m);
    m.registerObserver(&w);

    w.show();

    return a.exec();
}
