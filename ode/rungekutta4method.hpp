#ifndef RUNGEKUTTA4METHOD_HPP
#define RUNGEKUTTA4METHOD_HPP

#include "integrationmethod.hpp"

template <int SysOrder>
class RungeKutta4Method : public IntegrationMethod<SysOrder, 4> {
public:
    using typename IntegrationMethod<SysOrder, 4>::Point;
    typedef typename TrajectoryPoint<SysOrder>::Fparray Fparray;

    virtual ~RungeKutta4Method() {}

    virtual Point step(const CauchyProblem<SysOrder>& problem,
        const Point& oldPoint, double step) const {

        Fparray k1;
        for (int eqIndex = 0; eqIndex < SysOrder; ++eqIndex) {
            k1[eqIndex] = problem.rhsValueAt(eqIndex, oldPoint);
        }

        double halfStep = step * 0.5;
        Fparray vs;
        for (int eqIndex = 0; eqIndex < SysOrder; ++eqIndex) {
            vs[eqIndex] = oldPoint.u(eqIndex) + halfStep * k1[eqIndex];
        }
        const Point middlePoint1(oldPoint.x() + halfStep, vs);
        Fparray k2;
        for (int eqIndex = 0; eqIndex < SysOrder; ++eqIndex) {
            k2[eqIndex] = problem.rhsValueAt(eqIndex, middlePoint1);
        }

        for (int eqIndex = 0; eqIndex < SysOrder; ++eqIndex) {
            vs[eqIndex] = oldPoint.u(eqIndex) + halfStep * k2[eqIndex];
        }
        const Point middlePoint2(oldPoint.x() + halfStep, vs);
        Fparray k3;
        for (int eqIndex = 0; eqIndex < SysOrder; ++eqIndex) {
            k3[eqIndex] = problem.rhsValueAt(eqIndex, middlePoint2);
        }

        for (int eqIndex = 0; eqIndex < SysOrder; ++eqIndex) {
            vs[eqIndex] = oldPoint.u(eqIndex) + step * k3[eqIndex];
        }
        const Point middlePoint3(oldPoint.x() + step, vs);
        Fparray k4;
        for (int eqIndex = 0; eqIndex < SysOrder; ++eqIndex) {
            k4[eqIndex] = problem.rhsValueAt(eqIndex, middlePoint3);
        }

        double oneSixth = 1.0 / 6.0;
        Fparray finalVs;
        for (int eqIndex = 0; eqIndex < SysOrder; ++eqIndex) {
            finalVs[eqIndex] =
                oldPoint.u(eqIndex) + oneSixth * step *
                (k1[eqIndex] + 2 * (k2[eqIndex] + k3[eqIndex]) + k4[eqIndex]);
        }

        return Point(oldPoint.x() + step, finalVs);
    }
};


#endif // RUNGEKUTTA4METHOD_HPP

