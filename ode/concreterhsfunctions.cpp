#include <cmath>

#include "concreterhsfunctions.hpp"

double TestProblemRHSFunction::operator()(const RHSFunction::Point& p) const {
    return -2.5 * p.u(0);
}


double MainProblem1RHSFunction::operator()(const Point& p) const {
    double u = p.u(0);
    double x = p.x();

    return u * (1 + u * (log(1 + x) / (1 + x * x) - u * sin(10 * x)));
}


double MainProblem2RHSFunction1::operator()(const RHSFunction::Point& p) const {
    return p.u(1);
}


double MainProblem2RHSFunction2::operator()(const RHSFunction::Point& p) const {
    double u1 = p.u(0);
    double u2 = p.u(1);

    return -mA * u2 * u2 - mB * sin(u1);
}
