#ifndef ODEINTEGRATOR_HPP
#define ODEINTEGRATOR_HPP

#include <vector>
#include <memory>

#include "lecontroltype.hpp"
#include "cauchyproblem.hpp"
#include "integrationmethod.hpp"
#include "iterationinfo.hpp"

template <int SysOrder, int MethodOrder>
class ODEIntegrator {
public:
    typedef typename TrajectoryPoint<SysOrder>::Fparray Fparray;
    typedef IterationInfo<SysOrder> IterInfo;

    ODEIntegrator(const CauchyProblem<SysOrder>& cauchyProblem,
        double initialStep,
        const IntegrationMethod<SysOrder, MethodOrder>* method);

    const Fparray& stabilityCriterion() const;
    void setStabilityCriterion(const Fparray& criterion);

    int iterationsLimit() const;
    void setIterationsLimit(int limit);

    double rightBoundary() const;
    void setRightBoundary(double boundary);

    double boundaryEpsilon() const;
    void setBoundaryEpsilon(double epsilon);

    LEControlType leControlType() const;
    void setLEControlType(LEControlType leControlType);

    double leControlEpsilon() const;
    void setLEControlEpsilon(double epsilon);

    double lowerStepLimit() const;
    void setLowerStepLimit(double lowerStepLimit);

    double upperSteplimit() const;
    void setUpperStepLimit(double upperStepLimit);

    int iterationsCount() const;
    const IterInfo& iterationInfo(int index) const;
    const IterInfo& lastIterationInfo() const;

    bool runIteration();

    double maxLeEstimate() const;
    double maxLeEstimateX() const;
    double maxStep() const;
    double maxStepX() const;
    double minStep() const;
    double minStepX() const;

private:
    bool checkStopCondition() const;
    double getStep() const;

    LEControlType mLeControlType;
    double mLeControlEpsilon;

    int mIterationsLimit;
    Fparray mStabilityCriterion;
    double mRightBoundary;
    double mBoundaryEpsilon;
    double mLowerStepLimit;
    double mUpperStepLimit;

    CauchyProblem<SysOrder> mProblem;
    const IntegrationMethod<SysOrder, MethodOrder>* mMethod;

    std::vector<IterInfo> mSolvingStatistics;
    double mMaxLeEstimate;
    double mMaxLeEstimateX;
    double mMaxStep;
    double mMaxStepX;
    double mMinStep;
    double mMinStepX;
};

#include "odeintegrator.tpp"

#endif // ODEINTEGRATOR_HPP

