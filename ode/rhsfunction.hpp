#ifndef FUNCTION_HPP
#define FUNCTION_HPP

#include "trajectorypoint.hpp"

template <int Arity>
class RHSFunction {
public:
    typedef TrajectoryPoint<Arity - 1> Point;

    virtual ~RHSFunction() {}

    virtual double operator()(const Point& p) const = 0;
};


#endif // FUNCTION_HPP

