#ifndef INTEGRATIONMETHOD_HPP
#define INTEGRATIONMETHOD_HPP

#include "cauchyproblem.hpp"

template <int SysOrder, int MethodOrder>
class IntegrationMethod {
public:
    typedef TrajectoryPoint<SysOrder> Point;

    virtual ~IntegrationMethod() {}

    virtual Point step(const CauchyProblem<SysOrder>& problem,
        const Point& oldPoint, double step) const = 0;
    int order() const {
        return MethodOrder;
    }
};

#endif // INTEGRATIONMETHOD_HPP

