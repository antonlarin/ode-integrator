#ifndef CAUCHYPROBLEM_HPP
#define CAUCHYPROBLEM_HPP

#include "rhsfunction.hpp"

template <int Order>
class CauchyProblem {
public:
    typedef TrajectoryPoint<Order> Point;
    typedef std::array<RHSFunction<Order + 1>*, Order> RHSSet;

    CauchyProblem(const RHSSet& rhss, const Point& initCondition) :
        mRHSFunctions(rhss), mInitCondition(initCondition) {}

    double rhsValueAt(int index, const Point& args) const {
        assert(0 <= index && index < Order);

        return (*(mRHSFunctions[index]))(args);
    }

    const Point& initialCondition() const {
        return mInitCondition;
    }

private:
    const RHSSet mRHSFunctions;
    const Point mInitCondition;
};

#endif // CAUCHYPROBLEM_HPP

