#ifndef LECONTROLTYPE_HPP
#define LECONTROLTYPE_HPP

enum class LEControlType {
    UpAndDownControl, UpControl, DownControl, NoControl
};

#endif // LECONTROLTYPE_HPP

