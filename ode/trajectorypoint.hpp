#ifndef TRAJECTORYPOINT_HPP
#define TRAJECTORYPOINT_HPP

#include <cassert>
#include <array>

template <int Dim>
class TrajectoryPoint {
public:
    typedef std::array<double, Dim> Fparray;

    TrajectoryPoint(double x, const Fparray& us) : mX(x), mUs(us) {}
    TrajectoryPoint() : mX(0.0), mUs() {
        for (int i = 0; i < Dim; ++i) {
            mUs[i] = 0.0;
        }
    }

    double x() const {
        return mX;
    }

    double u(int i) const {
        assert(0 <= i && i < Dim);

        return mUs[i];
    }

    const Fparray& us() const {
        return mUs;
    }

private:
    double mX;
    Fparray mUs;
};

#endif // TRAJECTORYPOINT_HPP

