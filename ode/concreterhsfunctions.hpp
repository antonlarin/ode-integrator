#ifndef CONCRETERHSFUNCTIONS_HPP
#define CONCRETERHSFUNCTIONS_HPP

#include "rhsfunction.hpp"

class TestProblemRHSFunction : public RHSFunction<2> {
public:
    virtual ~TestProblemRHSFunction() {}

    virtual double operator()(const Point& p) const;
};

class MainProblem1RHSFunction: public RHSFunction<2> {
public:
    virtual ~MainProblem1RHSFunction() {}

    virtual double operator()(const Point& p) const;
};

class MainProblem2RHSFunction1: public RHSFunction<3> {
public:
    virtual ~MainProblem2RHSFunction1() {}

    virtual double operator()(const Point& p) const;
};

class MainProblem2RHSFunction2: public RHSFunction<3> {
public:
    MainProblem2RHSFunction2(double a, double b) : mA(a), mB(b) {}
    virtual ~MainProblem2RHSFunction2() {}

    virtual double operator()(const Point& p) const;

private:
    double mA;
    double mB;
};

#endif // CONCRETERHSFUNCTIONS_HPP

