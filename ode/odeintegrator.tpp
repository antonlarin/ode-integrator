#include <limits>

template <int SysOrder, int MethodOrder>
ODEIntegrator<SysOrder, MethodOrder>::ODEIntegrator(
        const CauchyProblem<SysOrder>& cauchyProblem, double initialStep,
        const IntegrationMethod<SysOrder, MethodOrder>* method) :
    mProblem(cauchyProblem), mMethod(method),
    mMaxLeEstimate(-std::numeric_limits<double>::infinity()),
    mMaxStep(-std::numeric_limits<double>::infinity()),
    mMinStep(std::numeric_limits<double>::infinity())
{
    IterInfo initialInfo = IterInfo(0, cauchyProblem.initialCondition(),
        initialStep);
    initialInfo.setStepIncreaseCount(0);
    initialInfo.setStepDecreaseCount(0);
    mSolvingStatistics.push_back(initialInfo);
}

template <int SysOrder, int MethodOrder>
const typename ODEIntegrator<SysOrder, MethodOrder>::Fparray&
    ODEIntegrator<SysOrder, MethodOrder>::stabilityCriterion() const {
    return mStabilityCriterion;
}
template <int SysOrder, int MethodOrder>
void ODEIntegrator<SysOrder, MethodOrder>::setStabilityCriterion(
    const Fparray& criterion) {
    for (int i = 0; i < SysOrder; ++i)
        assert(criterion[i] > 0);

    mStabilityCriterion = criterion;
}

template <int SysOrder, int MethodOrder>
int ODEIntegrator<SysOrder, MethodOrder>::iterationsLimit() const {
    return mIterationsLimit;
}
template <int SysOrder, int MethodOrder>
void ODEIntegrator<SysOrder, MethodOrder>::setIterationsLimit(int limit) {
    assert(limit > 0);

    mIterationsLimit = limit;
}

template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::rightBoundary() const {
    return mRightBoundary;
}
template <int SysOrder, int MethodOrder>
void ODEIntegrator<SysOrder, MethodOrder>::setRightBoundary(double boundary) {
    assert(boundary > mProblem.initialCondition().x());

    mRightBoundary = boundary;
}

template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::boundaryEpsilon() const {
    return mBoundaryEpsilon;
}
template <int SysOrder, int MethodOrder>
void ODEIntegrator<SysOrder, MethodOrder>::setBoundaryEpsilon(double epsilon) {
    assert(epsilon > 0);

    mBoundaryEpsilon = epsilon;
}

template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::lowerStepLimit() const {
    return mLowerStepLimit;
}
template <int SysOrder, int MethodOrder>
void ODEIntegrator<SysOrder, MethodOrder>::setLowerStepLimit(
    double lowerStepLimit)
{
    assert(0 < lowerStepLimit);

    mLowerStepLimit = lowerStepLimit;
}

template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::upperSteplimit() const {
    return mUpperStepLimit;
}
template <int SysOrder, int MethodOrder>
void ODEIntegrator<SysOrder, MethodOrder>::setUpperStepLimit(
    double upperStepLimit)
{
    assert(0 < upperStepLimit);

    mUpperStepLimit = upperStepLimit;
}

template <int SysOrder, int MethodOrder>
LEControlType ODEIntegrator<SysOrder, MethodOrder>::leControlType() const {
    return mLeControlType;
}
template <int SysOrder, int MethodOrder>
void ODEIntegrator<SysOrder, MethodOrder>::setLEControlType(
    LEControlType leControlType) {
    mLeControlType = leControlType;
}

template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::leControlEpsilon() const {
    return mLeControlEpsilon;
}
template <int SysOrder, int MethodOrder>
void ODEIntegrator<SysOrder, MethodOrder>::setLEControlEpsilon(double epsilon) {
    assert(epsilon > 0);

    mLeControlEpsilon = epsilon;
}

template <int SysOrder, int MethodOrder>
int ODEIntegrator<SysOrder, MethodOrder>::iterationsCount() const {
    return mSolvingStatistics.size();
}

template <int SysOrder, int MethodOrder>
const typename ODEIntegrator<SysOrder, MethodOrder>::IterInfo&
    ODEIntegrator<SysOrder, MethodOrder>::iterationInfo(
        int index) const {
    assert(0 <= index && index < iterationsCount());

    return mSolvingStatistics[index];
}

template <int SysOrder, int MethodOrder>
bool ODEIntegrator<SysOrder, MethodOrder>::runIteration() {
    const IterInfo& lastIteration = lastIterationInfo();
    double step = getStep();
    bool tryStepIncrease = lastIteration.shouldStepIncrease();
    int increaseCount = (tryStepIncrease) ? 1 : 0;
    int decreaseCount = 0;
    int attemptIndex = 0;

    bool stepIsNotGoodEnough = true;

    while (stepIsNotGoodEnough) {
        if (attemptIndex > 0) {
            step *= 0.5;
            if (tryStepIncrease) {
                tryStepIncrease = false;
                increaseCount = 0;
            } else {
                decreaseCount += 1;
            }
        }

        TrajectoryPoint<SysOrder> newPoint;
        newPoint = mMethod->step(mProblem, lastIteration.point(), step);
        IterInfo currentIteration(lastIteration.index() + 1, newPoint, step);
        if (mLeControlType != LEControlType::NoControl) {
            TrajectoryPoint<SysOrder> halfPoint =
                mMethod->step(mProblem, lastIteration.point(), step / 2);
            TrajectoryPoint<SysOrder> doubleHalfPoint;
            doubleHalfPoint = mMethod->step(mProblem, halfPoint, step / 2);
            currentIteration.setHalfDoubleStepValues(doubleHalfPoint.us());

            Fparray sValues;
            double sDenominator = pow(2, mMethod->order()) - 1;
            for (int eqIndex = 0; eqIndex < SysOrder; ++eqIndex) {
                sValues[eqIndex] =
                    (doubleHalfPoint.u(eqIndex) - newPoint.u(eqIndex)) /
                    sDenominator;
            }
            currentIteration.setSValues(sValues);
            double sNorm = currentIteration.sNorm();

            double leEstimateMultiplier = pow(2, mMethod->order());
            Fparray correctedValues;
            Fparray leEstimates;
            for (int eqIndex = 0; eqIndex < SysOrder; ++eqIndex) {
                leEstimates[eqIndex] = leEstimateMultiplier * sValues[eqIndex];
                correctedValues[eqIndex] =
                    newPoint.u(eqIndex) + leEstimates[eqIndex];
            }
            currentIteration.setLeEstimates(leEstimates);
            currentIteration.setCorrectedValues(correctedValues);

            double leUpperBound = mLeControlEpsilon;
            double leLowerBound =
                mLeControlEpsilon / pow(2, mMethod->order() + 1);

            switch (mLeControlType) {
            case LEControlType::UpAndDownControl: {
                if ((sNorm > leUpperBound) &&
                    ((step * 0.5) >= mLowerStepLimit)) {
                    attemptIndex += 1;
                } else {
                    currentIteration.setStepShouldIncrease(
                        sNorm < leLowerBound && 2 * step <= mUpperStepLimit);

                    stepIsNotGoodEnough = false;
                    currentIteration.setStepIncreaseCount(
                        lastIteration.stepIncreaseCount() + increaseCount);
                    currentIteration.setStepDecreaseCount(
                        lastIteration.stepDecreaseCount() + decreaseCount);
                    mSolvingStatistics.push_back(currentIteration);
                }
                break;
            }
            case LEControlType::UpControl: {
                if ((sNorm > leUpperBound) &&
                    ((step * 0.5) >= mLowerStepLimit)) {
                    attemptIndex += 1;
                } else {
                    stepIsNotGoodEnough = false;
                    currentIteration.setStepIncreaseCount(
                        lastIteration.stepIncreaseCount() + increaseCount);
                    currentIteration.setStepDecreaseCount(
                        lastIteration.stepDecreaseCount() + decreaseCount);
                    mSolvingStatistics.push_back(currentIteration);
                }
                break;
            }
            case LEControlType::DownControl: {
                currentIteration.setStepShouldIncrease(
                    sNorm < leLowerBound && 2 * step <= mUpperStepLimit);

                stepIsNotGoodEnough = false;
                currentIteration.setStepIncreaseCount(
                    lastIteration.stepIncreaseCount() + increaseCount);
                currentIteration.setStepDecreaseCount(
                    lastIteration.stepDecreaseCount() + decreaseCount);
                mSolvingStatistics.push_back(currentIteration);
                break;
            }
            default:
                break;
            }
        } else {
            stepIsNotGoodEnough = false;
            currentIteration.setStepIncreaseCount(
                lastIteration.stepIncreaseCount());
            currentIteration.setStepDecreaseCount(
                lastIteration.stepDecreaseCount());
            mSolvingStatistics.push_back(currentIteration);
        }
    }

    const IterInfo& currentIteration = lastIterationInfo();
    if (currentIteration.leEstimate(SysOrder - 1) > mMaxLeEstimate) {
        mMaxLeEstimate = currentIteration.leEstimate(SysOrder - 1);
        mMaxLeEstimateX = currentIteration.point().x();
    }
    if (currentIteration.step() > mMaxStep) {
        mMaxStep = currentIteration.step();
        mMaxStepX = currentIteration.point().x();
    }
    if (currentIteration.step() < mMinStep) {
        mMinStep = currentIteration.step();
        mMinStepX = currentIteration.point().x();
    }

    return checkStopCondition();
}

template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::maxLeEstimate() const {
    return mMaxLeEstimate;
}
template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::maxLeEstimateX() const {
    return mMaxLeEstimateX;
}
template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::maxStep() const {
    return mMaxStep;
}
template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::maxStepX() const {
    return mMaxStepX;
}
template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::minStep() const {
    return mMinStep;
}
template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::minStepX() const {
    return mMinStepX;
}


template <int SysOrder, int MethodOrder>
const typename ODEIntegrator<SysOrder, MethodOrder>::IterInfo&
    ODEIntegrator<SysOrder, MethodOrder>::lastIterationInfo() const {
    return mSolvingStatistics.back();
}

template <int SysOrder, int MethodOrder>
bool ODEIntegrator<SysOrder, MethodOrder>::checkStopCondition() const {
    const IterInfo& lastIteration = lastIterationInfo();
        return lastIteration.index() == mIterationsLimit ||
            lastIteration.point().x() > mRightBoundary - mBoundaryEpsilon;
}

template <int SysOrder, int MethodOrder>
double ODEIntegrator<SysOrder, MethodOrder>::getStep() const {
    const IterInfo& lastIteration = lastIterationInfo();

    if (lastIteration.shouldStepIncrease()) {
        return lastIteration.step() * 2;
    } else {
        return lastIteration.step();
    }
}
