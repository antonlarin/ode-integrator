#ifndef ITERATIONINFO_HPP
#define ITERATIONINFO_HPP

#include <cmath>

#include "trajectorypoint.hpp"

template <int SysOrder>
class IterationInfo {
public:
    typedef TrajectoryPoint<SysOrder> Point;
    typedef typename TrajectoryPoint<SysOrder>::Fparray Fparray;

    IterationInfo(int index, const Point& point, double step) :
        mIndex(index), mStep(step), mPoint(point), mStepShouldIncrease(false) {}

    int index() const {
        return mIndex;
    }

    const Point& point() const {
        return mPoint;
    }

    double step() const {
        return mStep;
    }

    double halfDoubleStepValue(int index) const {
        assert(0 <= index && index <= SysOrder);

        return mHalfDoubleStepValues[index];
    }
    const Fparray& halfDoubleStepValues() const {
        return mHalfDoubleStepValues;
    }
    void setHalfDoubleStepValues(const Fparray& values) {
        mHalfDoubleStepValues = values;
    }

    double sValue(int index) const {
        assert(0 <= index && index <= SysOrder);

        return mS[index];
    }
    const Fparray& sValues() const {
        return mS;
    }
    void setSValues(const Fparray& values) {
        mS = values;
    }
    double sNorm() const {
        double norm = fabs(mS[0]);

        for (int eqIndex = 1; eqIndex < SysOrder; ++eqIndex) {
            norm = std::max(norm, fabs(mS[eqIndex]));
        }

        return norm;
    }

    double leEstimate(int index) const {
        assert(0 <= index && index <= SysOrder);

        return mLeEstimates[index];
    }
    void setLeEstimates(const Fparray& values) {
        mLeEstimates = values;
    }

    const Fparray& correctedValues() const {
        return mCorrectedValues;
    }
    void setCorrectedValues(const Fparray& values) {
        mCorrectedValues = values;
    }

    int stepIncreaseCount() const {
        return mStepIncreaseCount;
    }
    void setStepIncreaseCount(int count) {
        mStepIncreaseCount = count;
    }

    int stepDecreaseCount() const {
        return mStepDecreaseCount;
    }
    void setStepDecreaseCount(int count) {
        mStepDecreaseCount = count;
    }

    bool shouldStepIncrease() const {
        return mStepShouldIncrease;
    }
    void setStepShouldIncrease(bool shouldIncrease) {
        mStepShouldIncrease = shouldIncrease;
    }

private:
    const int mIndex;
    const double mStep;
    const Point mPoint;
    Fparray mHalfDoubleStepValues;
    Fparray mS;
    Fparray mLeEstimates;
    Fparray mCorrectedValues;
    int mStepIncreaseCount;
    int mStepDecreaseCount;
    bool mStepShouldIncrease;
};

#endif // ITERATIONINFO_HPP

