#ifndef ODESPROGRESSDIALOG_HPP
#define ODESPROGRESSDIALOG_HPP

#include "ui_odesprogressdialog.h"

#include "odesmodel.hpp"

class ODESProgressDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ODESProgressDialog(ODESModel& model, QWidget *parent = 0);

    void initialize();

public slots:
    void show();
    void update(int latestIndex);
    void pauseRunRequested();

private:
    void connectSlots();

    Ui::ODESProgressDialog mUi;
    ODESModel& mModel;
};

#endif // ODESPROGRESSDIALOG_HPP
