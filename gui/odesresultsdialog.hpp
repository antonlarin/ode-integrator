#ifndef ODESRESULTSDIALOG_HPP
#define ODESRESULTSDIALOG_HPP

#include "ui_odesresultsdialog.h"

#include "odesmodel.hpp"

class ODESResultsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ODESResultsDialog(ODESModel& model, QWidget *parent = 0);

    void initialize();

private:
    Ui::ODESResultsDialog mUi;
    ODESModel& mModel;
};

#endif // ODESRESULTSDIALOG_HPP
