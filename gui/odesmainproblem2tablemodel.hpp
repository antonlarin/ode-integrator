#ifndef ODESMAINPROBLEM2TABLEMODEL_HPP
#define ODESMAINPROBLEM2TABLEMODEL_HPP

#include <QAbstractTableModel>
#include "odesmodel.hpp"

class ODESMainProblem2TableModel : public QAbstractTableModel
{
public:
    ODESMainProblem2TableModel(ODESModel& model);
    virtual ~ODESMainProblem2TableModel();

    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant headerData(int section,
        Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data(const QModelIndex & index,
        int role = Qt::DisplayRole) const;

private:
    ODESModel& mModel;
};

#endif // ODESMAINPROBLEM2TABLEMODEL_HPP
