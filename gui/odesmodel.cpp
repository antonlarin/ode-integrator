#include <cassert>
#include <chrono>

#include "odesmodel.hpp"
#include "ode/concreterhsfunctions.hpp"
#include "ode/rungekutta4method.hpp"

ODESModel::ODESModel(QObject* parent) :
    QObject(parent),
    mTestProblemIntegrator(nullptr),
    mMainProblem1Integrator(nullptr),
    mMainProblem2Integrator(nullptr),
    mScalarMethod(new RungeKutta4Method<1>()),
    mSystemMethod(new RungeKutta4Method<2>()),
    mMainProblem2RHSFunction2(nullptr),
    mProblemType(ProblemType::TEST_PROBLEM), mA(1), mB(1),
    mXInitial(0.0), mUInitial(1), mUPrimeInitial(0), mInitialStep(1e-3),
    mLeControlType(LEControlType::UpAndDownControl), mLeControlEpsilon(1e-6),
    mLowerStepLimit(1e-10), mUpperStepLimit(10),
    mNMax(10000), mRightBoundary(300), mBoundaryEpsilon(0.1)
{
    moveToThread(&mRunner);
    mRunner.start();
}

ODESModel::~ODESModel()
{
    mRunner.exit();
    mRunner.wait();
}

ProblemType ODESModel::problemType() const {
    return mProblemType;
}

void ODESModel::setProblemType(ProblemType problemType) {
    mProblemType = problemType;
    notify();
}


double ODESModel::a() const {
    return mA;
}
void ODESModel::setA(double a) {
    if (0 < a) {
        mA = a;
    }
}

double ODESModel::b() const {
    return mB;
}
void ODESModel::setB(double b) {
    if (0 < b) {
        mB = b;
    }
}

double ODESModel::xInitial() const {
    return mXInitial;
}
void ODESModel::setXInitial(double xInitial) {
    mXInitial = xInitial;
}

double ODESModel::uInitial() const {
    return mUInitial;
}
void ODESModel::setUInitial(double uInitial) {
    mUInitial = uInitial;
}

double ODESModel::uPrimeInitial() const {
    return mUPrimeInitial;
}
void ODESModel::setUPrimeInitial(double uPrimeInitial) {
    mUPrimeInitial = uPrimeInitial;
}

double ODESModel::initialStep() const {
    return mInitialStep;
}
void ODESModel::setInitialStep(double initialStep) {
    if (0 < initialStep) {
        mInitialStep = initialStep;
    }
}

LEControlType ODESModel::leControlType() const {
    return mLeControlType;
}
void ODESModel::setLeControlType(LEControlType leControlType) {
    mLeControlType = leControlType;
}

double ODESModel::leControlEpsilon() const {
    return mLeControlEpsilon;
}
void ODESModel::setLeControlEpsilon(double leControlEpsilon) {
    if (0 < leControlEpsilon) {
        mLeControlEpsilon = leControlEpsilon;
    }
}

double ODESModel::lowerStepLimit() const {
    return mLowerStepLimit;
}

void ODESModel::setLowerStepLimit(double lowerStepLimit) {
    if (0 < lowerStepLimit) {
        mLowerStepLimit = lowerStepLimit;
    }
}

double ODESModel::upperStepLimit() const {
    return mUpperStepLimit;
}

void ODESModel::setUpperStepLimit(double upperStepLimit) {
    if (0 < upperStepLimit) {
        mUpperStepLimit = upperStepLimit;
    }
}

int ODESModel::nMax() const {
    return mNMax;
}
void ODESModel::setNMax(int nMax) {
    if (0 < nMax) {
        mNMax = nMax;
    }
}

double ODESModel::rightBoundary() const {
    return mRightBoundary;
}
void ODESModel::setRightBoundary(double rightBoundary) {
    mRightBoundary = rightBoundary;
}

double ODESModel::boundaryEpsilon() const {
    return mBoundaryEpsilon;
}
void ODESModel::setBoundaryEpsilon(double boundaryEpsilon) {
    assert(0 < boundaryEpsilon);

    mBoundaryEpsilon = boundaryEpsilon;
}


void ODESModel::startRun() {
    switch (mProblemType) {
    case ProblemType::TEST_PROBLEM: {
        startTestProblemRun();
        break;
    }
    case ProblemType::MAIN_PROBLEM1: {
        startMainProblem1Run();
        break;
    }
    case ProblemType::MAIN_PROBLEM2: {
        startMainProblem2Run();
        break;
    }
    }
}

bool ODESModel::running() const {
    return mRunning;
}

double ODESModel::testProblemAnalyticSolution(double x) const {
    return exp(2.5 * (mXInitial - x)) * mUInitial;
}

double ODESModel::maxDifferenceWithAnalyticSolution() const {
    return mMaxDifferenceWithAnalyticSolution;
}

double ODESModel::maxDifferenceWithAnalyticSolutionX() const {
    return mMaxDifferenceWithAnalyticSolutionX;
}

void ODESModel::pauseRun() {
    mRunning = false;
}

void ODESModel::startTestProblemRun() {
    typename CauchyProblem<1>::RHSSet rhsSet = {{&mTestProblemRHSFunction}};
    typename TrajectoryPoint<1>::Fparray initialU = {{mUInitial}};
    typename CauchyProblem<1>::Point initConditions(mXInitial, initialU);
    CauchyProblem<1> problem(rhsSet, initConditions);

    mTestProblemIntegrator.reset(new ODEIntegrator<1, 4>(problem, mInitialStep,
        mScalarMethod.get()));

    mTestProblemIntegrator->setIterationsLimit(mNMax);
    mTestProblemIntegrator->setRightBoundary(mRightBoundary);
    mTestProblemIntegrator->setBoundaryEpsilon(mBoundaryEpsilon);
    mTestProblemIntegrator->setLEControlType(mLeControlType);
    mTestProblemIntegrator->setLEControlEpsilon(mLeControlEpsilon);
    mTestProblemIntegrator->setLowerStepLimit(mLowerStepLimit);
    mTestProblemIntegrator->setUpperStepLimit(mUpperStepLimit);

    mMaxDifferenceWithAnalyticSolution =
        -std::numeric_limits<double>::infinity();
    mMaxDifferenceWithAnalyticSolutionX = 0.0;

    mRunning = true;
    bool stopCondition = false;

    using namespace std::chrono;
    steady_clock::time_point start = steady_clock::now();

    while (!stopCondition && mRunning) {
        stopCondition = mTestProblemIntegrator->runIteration();
        double newX = mTestProblemIntegrator->lastIterationInfo().point().x();
        double newV = mTestProblemIntegrator->lastIterationInfo().point().u(0);
        if (fabs(newV - testProblemAnalyticSolution(newX)) >
            mMaxDifferenceWithAnalyticSolution) {
            mMaxDifferenceWithAnalyticSolution =
                fabs(newV - testProblemAnalyticSolution(newX));
            mMaxDifferenceWithAnalyticSolutionX = newX;
        }

        steady_clock::time_point middle = steady_clock::now();
        milliseconds timeSpan = duration_cast<milliseconds>(middle - start);

        if (timeSpan.count() > milliseconds(300).count()) {
            emit runInfoChanged(
                mTestProblemIntegrator->lastIterationInfo().index());
            start = steady_clock::now();
        }
    }

    mRunning = false;
    emit runInfoChanged(mTestProblemIntegrator->lastIterationInfo().index());
    emit runFinished();
}

void ODESModel::startMainProblem1Run()
{
    typename CauchyProblem<1>::RHSSet rhsSet =
        {{&mMainProblem1RHSFunction}};
    typename TrajectoryPoint<1>::Fparray initialU = {{mUInitial}};
    typename CauchyProblem<1>::Point initConditions(mXInitial, initialU);
    CauchyProblem<1> problem(rhsSet, initConditions);

    mMainProblem1Integrator.reset(new ODEIntegrator<1, 4>(problem, mInitialStep,
        mScalarMethod.get()));

    mMainProblem1Integrator->setIterationsLimit(mNMax);
    mMainProblem1Integrator->setRightBoundary(mRightBoundary);
    mMainProblem1Integrator->setBoundaryEpsilon(mBoundaryEpsilon);
    mMainProblem1Integrator->setLEControlType(mLeControlType);
    mMainProblem1Integrator->setLEControlEpsilon(mLeControlEpsilon);
    mMainProblem1Integrator->setLowerStepLimit(mLowerStepLimit);
    mMainProblem1Integrator->setUpperStepLimit(mUpperStepLimit);

    mRunning = true;
    bool stopCondition = false;

    using namespace std::chrono;
    steady_clock::time_point start = steady_clock::now();

    while (!stopCondition && mRunning) {
        stopCondition = mMainProblem1Integrator->runIteration();
        steady_clock::time_point middle = steady_clock::now();
        milliseconds timeSpan = duration_cast<milliseconds>(middle - start);

        if (timeSpan.count() > milliseconds(300).count()) {
            emit runInfoChanged(
                mMainProblem1Integrator->lastIterationInfo().index());
            start = steady_clock::now();
        }
    }

    mRunning = false;
    emit runInfoChanged(mMainProblem1Integrator->lastIterationInfo().index());
    emit runFinished();
}

void ODESModel::startMainProblem2Run()
{
    mMainProblem2RHSFunction2.reset(new MainProblem2RHSFunction2(mA, mB));
    typename CauchyProblem<2>::RHSSet rhsSet =
        {{&mMainProblem2RHSFunction1, mMainProblem2RHSFunction2.get()}};
    typename TrajectoryPoint<2>::Fparray initialU =
        {{mUInitial, mUPrimeInitial}};
    typename CauchyProblem<2>::Point initConditions(mXInitial, initialU);
    CauchyProblem<2> problem(rhsSet, initConditions);

    mMainProblem2Integrator.reset(new ODEIntegrator<2, 4>(problem,
        mInitialStep, mSystemMethod.get()));

    mMainProblem2Integrator->setIterationsLimit(mNMax);
    mMainProblem2Integrator->setRightBoundary(mRightBoundary);
    mMainProblem2Integrator->setBoundaryEpsilon(mBoundaryEpsilon);
    mMainProblem2Integrator->setLEControlType(mLeControlType);
    mMainProblem2Integrator->setLEControlEpsilon(mLeControlEpsilon);
    mMainProblem2Integrator->setLowerStepLimit(mLowerStepLimit);
    mMainProblem2Integrator->setUpperStepLimit(mUpperStepLimit);

    mRunning = true;
    bool stopCondition = false;

    using namespace std::chrono;
    steady_clock::time_point start = steady_clock::now();

    while (!stopCondition && mRunning) {
        stopCondition = mMainProblem2Integrator->runIteration();
        steady_clock::time_point middle = steady_clock::now();
        milliseconds timeSpan = duration_cast<milliseconds>(middle - start);

        if (timeSpan.count() > milliseconds(300).count()) {
            emit runInfoChanged(
                mMainProblem2Integrator->lastIterationInfo().index());
            start = steady_clock::now();
        }
    }

    mRunning = false;
    emit runInfoChanged(mMainProblem2Integrator->lastIterationInfo().index());
    emit runFinished();
}

ODESModel::ScalarIntegrator* ODESModel::testProblemIntegrator() {
    return mTestProblemIntegrator.get();
}

ODESModel::ScalarIntegrator*ODESModel::mainProblem1Integrator() {
    return mMainProblem1Integrator.get();
}

ODESModel::SystemIntegrator*ODESModel::mainProblem2Integrator() {
    return mMainProblem2Integrator.get();
}
