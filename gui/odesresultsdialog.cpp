#include "guiutilities.hpp"
#include "odesresultsdialog.hpp"

ODESResultsDialog::ODESResultsDialog(ODESModel& model, QWidget *parent) :
    QDialog(parent), mModel(model)
{
    mUi.setupUi(this);
}

void ODESResultsDialog::initialize() {
    int index;
    double lastX;
    double maxLeEstimate;
    double maxLeEstimateX;
    int stepIncreaseCount;
    int stepDecreaseCount;
    double maxStep;
    double maxStepX;
    double minStep;
    double minStepX;

    switch (mModel.problemType()) {
    case ProblemType::TEST_PROBLEM: {
        ODEIntegrator<1, 4>* integrator = mModel.testProblemIntegrator();

        index = integrator->lastIterationInfo().index();
        lastX = integrator->lastIterationInfo().point().x();
        maxLeEstimate = integrator->maxLeEstimate();
        maxLeEstimateX = integrator->maxLeEstimateX();
        stepIncreaseCount = integrator->lastIterationInfo().stepIncreaseCount();
        stepDecreaseCount = integrator->lastIterationInfo().stepDecreaseCount();
        maxStep = integrator->maxStep();
        maxStepX = integrator->maxStepX();
        minStep = integrator->minStep();
        minStepX = integrator->minStepX();
        break;
    }
    case ProblemType::MAIN_PROBLEM1: {
        ODEIntegrator<1, 4>* integrator = mModel.mainProblem1Integrator();

        index = integrator->lastIterationInfo().index();
        lastX = integrator->lastIterationInfo().point().x();
        maxLeEstimate = integrator->maxLeEstimate();
        maxLeEstimateX = integrator->maxLeEstimateX();
        stepIncreaseCount = integrator->lastIterationInfo().stepIncreaseCount();
        stepDecreaseCount = integrator->lastIterationInfo().stepDecreaseCount();
        maxStep = integrator->maxStep();
        maxStepX = integrator->maxStepX();
        minStep = integrator->minStep();
        minStepX = integrator->minStepX();
        break;
    }
    case ProblemType::MAIN_PROBLEM2: {
        ODEIntegrator<2, 4>* integrator = mModel.mainProblem2Integrator();

        index = integrator->lastIterationInfo().index();
        lastX = integrator->lastIterationInfo().point().x();
        maxLeEstimate = integrator->maxLeEstimate();
        maxLeEstimateX = integrator->maxLeEstimateX();
        stepIncreaseCount = integrator->lastIterationInfo().stepIncreaseCount();
        stepDecreaseCount = integrator->lastIterationInfo().stepDecreaseCount();
        maxStep = integrator->maxStep();
        maxStepX = integrator->maxStepX();
        minStep = integrator->minStep();
        minStepX = integrator->minStepX();
        break;
    }
    }

    mUi.iterationsCountLabel->setText(QString("N = %1").arg(index));
    mUi.bMinuslastXLabel->setText(
        indexTemplate.arg("b - x", "N", " = %1").
        arg(mModel.rightBoundary() - lastX));
    mUi.maxSLabel->setText(
        QString("max|ОЛП| = %1 при x = %2").arg(maxLeEstimate).
        arg(maxLeEstimateX));
    mUi.stepIncreaseCountLabel->setText(
        QString("Удвоений шага: %1").arg(stepIncreaseCount));
    mUi.stepDecreaseCountLabel->setText(
        QString("Делений шага: %1").arg(stepDecreaseCount));
    mUi.maxStepLabel->setText(
        indexTemplate.arg("max h", "i", " = %1 при ").
        append(indexTemplate.arg("x", "i+1", " = %2")).arg(maxStep).
        arg(maxStepX));
    mUi.minStepLabel->setText(
        indexTemplate.arg("min h", "i", " = %1 при ").
        append(indexTemplate.arg("x", "i+1", " = %2")).arg(minStep).
        arg(minStepX));

    if (mModel.problemType() != ProblemType::TEST_PROBLEM) {
        mUi.maxDifferenceWithAnalyticSolutionLabel->setVisible(false);
    } else {
        double maxDifferenceWithAnalyticSolution =
            mModel.maxDifferenceWithAnalyticSolution();
        double maxDifferenceWithAnalyticSolutionX =
            mModel.maxDifferenceWithAnalyticSolutionX();
        QString test = indexTemplate.arg("max |u", "i", " - ").
                append(indexTemplate.arg("v", "i", "| = %1 при ")).
                append(indexTemplate.arg("x", "i", " = %2"));
        mUi.maxDifferenceWithAnalyticSolutionLabel->setText(
            indexTemplate.arg("max |u", "i", " - ").
            append(indexTemplate.arg("v", "i", "| = %1 при ")).
            append(indexTemplate.arg("x", "i", " = %2")).
            arg(maxDifferenceWithAnalyticSolution).
            arg(maxDifferenceWithAnalyticSolutionX));
    }
}
