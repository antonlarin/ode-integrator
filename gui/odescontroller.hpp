#ifndef ODESCONTROLLER_HPP
#define ODESCONTROLLER_HPP

#include "odesmodel.hpp"

class ODESController {
public:
    ODESController(ODESModel& model);

    ODESModel& model();

private:
    ODESModel& mModel;
};

#endif // ODESCONTROLLER_HPP
