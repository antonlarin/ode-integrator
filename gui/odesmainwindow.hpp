#ifndef ODESMAINWINDOW_HPP
#define ODESMAINWINDOW_HPP

#include <memory>

#include "ui_odesmainwindow.h"

#include "observer.hpp"
#include "odescontroller.hpp"
#include "odesprogressdialog.hpp"
#include "odesresultsdialog.hpp"
#include "odestestproblemtablemodel.hpp"
#include "odesmainproblem1tablemodel.hpp"
#include "odesmainproblem2tablemodel.hpp"

class ODESMainWindow : public QMainWindow, public Observer
{
    Q_OBJECT

public:
    explicit ODESMainWindow(ODESModel& model, QWidget *parent = 0);
    virtual ~ODESMainWindow() {}

    virtual void update();

    void setProgressDialog(ODESProgressDialog& progressDialog);

public slots:
    void problemTypeActiveRadioChanged();

    void aEdited(QString value);
    void bEdited(QString value);
    void xInitialEdited(QString value);
    void uInitialEdited(QString value);
    void uPrimeInitialEdited(QString value);
    void initialStepEdited(QString value);

    void leControlEpsilonEdited(QString value);
    void leControlTypeActiveRadioChanged();
    void lowerStepLimitEdited(QString value);
    void upperStepLimitEdited(QString value);

    void nMaxEdited(QString value);
    void rightBoundaryEdited(QString value);
    void boundaryEpsilonEdited(QString value);

    void startRunClicked();
    void runFinishUpdate();

    void updateData(int latestIndex);

private:
    void connectSlots();
    void initializePlots();
    void clearTestProblemPlot();
    void clearMainProblem1Plot();
    void clearMainProblem2Plots();

    void updateEditsContents();

    void updateTestProblemPlot(int latestIndex);
    void updateMainProblem1Plot(int latestIndex);
    void updateMainProblem2Plots(int latestIndex);

    void updateTestProblemTable(int latestIndex);
    void updateMainProblem1Table(int latestIndex);
    void updateMainProblem2Table(int latestIndex);

    Ui::ODESMainWindow mUi;
    ODESModel& mModel;
    std::unique_ptr<ODESProgressDialog> mProgressDialog;
    std::unique_ptr<ODESResultsDialog> mResultsDialog;
    std::unique_ptr<ODESTestProblemTableModel> mTestProblemTableModel;
    std::unique_ptr<ODESMainProblem1TableModel> mMainProblem1TableModel;
    std::unique_ptr<ODESMainProblem2TableModel> mMainProblem2TableModel;
    QCPCurve* mPhasePlaneCurve;

    int mPreviouslyLastIterationIndex;
};

#endif // ODESMAINWINDOW_HPP
