#include "odesmainwindow.hpp"

ODESMainWindow::ODESMainWindow(ODESModel& model, QWidget *parent) :
    QMainWindow(parent), mModel(model),
    mProgressDialog(new ODESProgressDialog(mModel)),
    mResultsDialog(new ODESResultsDialog(mModel)),
    mTestProblemTableModel(new ODESTestProblemTableModel(mModel)),
    mMainProblem1TableModel(new ODESMainProblem1TableModel(mModel)),
    mMainProblem2TableModel(new ODESMainProblem2TableModel(mModel)),
    mPhasePlaneCurve(nullptr)
{
    mUi.setupUi(this);

    connectSlots();
    initializePlots();
    mUi.testProblemTable->setModel(mTestProblemTableModel.get());
    mUi.mainProblem1Table->setModel(mMainProblem1TableModel.get());
    mUi.mainProblem2Table->setModel(mMainProblem2TableModel.get());
    update();
}

void ODESMainWindow::update() {
    updateEditsContents();
}

void ODESMainWindow::problemTypeActiveRadioChanged() {
    if (mUi.testProblemRadio->isChecked()) {
        mModel.setProblemType(ProblemType::TEST_PROBLEM);
        mUi.outputStack->setCurrentIndex(0);
    } else if (mUi.mainProblem1Radio->isChecked()) {
        mModel.setProblemType(ProblemType::MAIN_PROBLEM1);
        mUi.outputStack->setCurrentIndex(1);
    } else { // mUi.mainProblem2Radio->isChecked()
        mModel.setProblemType(ProblemType::MAIN_PROBLEM2);
        mUi.outputStack->setCurrentIndex(2);
    }
}


void ODESMainWindow::aEdited(QString value) {
    bool isValidDouble = true;
    double newValue = value.toDouble(&isValidDouble);
    if (isValidDouble) {
         mModel.setA(newValue);
    }
}

void ODESMainWindow::bEdited(QString value) {
    bool isValidDouble = true;
    double newValue = value.toDouble(&isValidDouble);
    if (isValidDouble) {
         mModel.setB(newValue);
    }
}

void ODESMainWindow::xInitialEdited(QString value) {
    bool isValidDouble = true;
    double newValue = value.toDouble(&isValidDouble);
    if (isValidDouble) {
         mModel.setXInitial(newValue);
    }
}

void ODESMainWindow::uInitialEdited(QString value) {
    bool isValidDouble = true;
    double newValue = value.toDouble(&isValidDouble);
    if (isValidDouble) {
         mModel.setUInitial(newValue);
    }
}

void ODESMainWindow::uPrimeInitialEdited(QString value) {
    bool isValidDouble = true;
    double newValue = value.toDouble(&isValidDouble);
    if (isValidDouble) {
         mModel.setUPrimeInitial(newValue);
    }
}

void ODESMainWindow::initialStepEdited(QString value) {
    bool isValidDouble = true;
    double newValue = value.toDouble(&isValidDouble);
    if (isValidDouble) {
         mModel.setInitialStep(newValue);
    }
}

void ODESMainWindow::leControlEpsilonEdited(QString value) {
    bool isValidDouble = true;
    double newValue = value.toDouble(&isValidDouble);
    if (isValidDouble) {
         mModel.setLeControlEpsilon(newValue);
    }
}

void ODESMainWindow::leControlTypeActiveRadioChanged() {
    if (mUi.upAndDownLEControlRButton->isChecked()) {
        mModel.setLeControlType(LEControlType::UpAndDownControl);
    } else if (mUi.upLEControlRButton->isChecked()) {
        mModel.setLeControlType(LEControlType::UpControl);
    } else if (mUi.downLEControlRButton->isChecked()) {
        mModel.setLeControlType(LEControlType::DownControl);
    } else { // mUi.noControlRButton->isChecked()
        mModel.setLeControlType(LEControlType::NoControl);
    }
}

void ODESMainWindow::upperStepLimitEdited(QString value) {
    bool isValidDouble = true;
    double newValue = value.toDouble(&isValidDouble);
    if (isValidDouble) {
         mModel.setUpperStepLimit(newValue);
    }
}

void ODESMainWindow::lowerStepLimitEdited(QString value) {
    bool isValidDouble = true;
    double newValue = value.toDouble(&isValidDouble);
    if (isValidDouble) {
         mModel.setLowerStepLimit(newValue);
    }
}

void ODESMainWindow::nMaxEdited(QString value) {
    bool isValidInteger = true;
    int newValue = value.toInt(&isValidInteger);
    if (isValidInteger) {
        mModel.setNMax(newValue);
    }
}

void ODESMainWindow::rightBoundaryEdited(QString value) {
    bool isValidDouble = true;
    double newValue = value.toDouble(&isValidDouble);
    if (isValidDouble) {
         mModel.setRightBoundary(newValue);
    }
}

void ODESMainWindow::boundaryEpsilonEdited(QString value) {
    bool isValidDouble = true;
    double newValue = value.toDouble(&isValidDouble);
    if (isValidDouble) {
         mModel.setBoundaryEpsilon(newValue);
    }
}

void ODESMainWindow::startRunClicked() {
    mPreviouslyLastIterationIndex = 0;

    switch (mModel.problemType()) {
    case ProblemType::TEST_PROBLEM:
        clearTestProblemPlot();
        break;
    case ProblemType::MAIN_PROBLEM1:
        clearMainProblem1Plot();
        break;
    case ProblemType::MAIN_PROBLEM2:
        clearMainProblem2Plots();
        break;
    }

    mProgressDialog->show();
}

void ODESMainWindow::runFinishUpdate() {
    mUi.showResultButton->setEnabled(true);
    mResultsDialog->initialize();
    mResultsDialog->show();

    int latestIndex;
    switch (mModel.problemType()) {
    case ProblemType::TEST_PROBLEM:
        latestIndex = mModel.testProblemIntegrator()->iterationsCount() - 1;
        break;
    case ProblemType::MAIN_PROBLEM1:
        latestIndex = mModel.mainProblem1Integrator()->iterationsCount() - 1;
        break;
    case ProblemType::MAIN_PROBLEM2:
        latestIndex = mModel.mainProblem2Integrator()->iterationsCount() - 1;
        break;
    }

    updateData(latestIndex);
}

void ODESMainWindow::updateData(int latestIndex) {
    switch (mModel.problemType()) {
    case ProblemType::TEST_PROBLEM:
        updateTestProblemPlot(latestIndex);
        updateTestProblemTable(latestIndex);
        break;
    case ProblemType::MAIN_PROBLEM1:
        updateMainProblem1Plot(latestIndex);
        updateMainProblem1Table(latestIndex);
        break;
    case ProblemType::MAIN_PROBLEM2:
        updateMainProblem2Plots(latestIndex);
        updateMainProblem2Table(latestIndex);
        break;
    }

    mPreviouslyLastIterationIndex = latestIndex;
}

void ODESMainWindow::connectSlots() {
    connect(mUi.testProblemRadio, SIGNAL(clicked()),
        this, SLOT(problemTypeActiveRadioChanged()));
    connect(mUi.mainProblem1Radio, SIGNAL(clicked()),
        this, SLOT(problemTypeActiveRadioChanged()));
    connect(mUi.mainProblem2Radio, SIGNAL(clicked()),
        this, SLOT(problemTypeActiveRadioChanged()));

    connect(mUi.aEdit, SIGNAL(textEdited(QString)),
        this, SLOT(aEdited(QString)));
    connect(mUi.bEdit, SIGNAL(textEdited(QString)),
        this, SLOT(bEdited(QString)));
    connect(mUi.x0Edit, SIGNAL(textEdited(QString)),
        this, SLOT(xInitialEdited(QString)));
    connect(mUi.u0Edit, SIGNAL(textEdited(QString)),
        this, SLOT(uInitialEdited(QString)));
    connect(mUi.uPrime0Edit, SIGNAL(textEdited(QString)),
        this, SLOT(uPrimeInitialEdited(QString)));
    connect(mUi.h0Edit, SIGNAL(textEdited(QString)),
        this, SLOT(initialStepEdited(QString)));

    connect(mUi.LEEpsilonEdit, SIGNAL(textEdited(QString)),
        this, SLOT(leControlEpsilonEdited(QString)));
    connect(mUi.upAndDownLEControlRButton, SIGNAL(clicked()),
        this, SLOT(leControlTypeActiveRadioChanged()));
    connect(mUi.upLEControlRButton, SIGNAL(clicked()),
        this, SLOT(leControlTypeActiveRadioChanged()));
    connect(mUi.downLEControlRButton, SIGNAL(clicked()),
        this, SLOT(leControlTypeActiveRadioChanged()));
    connect(mUi.noLEControlRButton, SIGNAL(clicked()),
        this, SLOT(leControlTypeActiveRadioChanged()));
    connect(mUi.lowerStepLimitEdit, SIGNAL(textEdited(QString)),
        this, SLOT(lowerStepLimitEdited(QString)));
    connect(mUi.upperStepLimitEdit, SIGNAL(textEdited(QString)),
        this, SLOT(upperStepLimitEdited(QString)));

    connect(mUi.nMaxEdit, SIGNAL(textEdited(QString)),
        this, SLOT(nMaxEdited(QString)));
    connect(mUi.rightBoundaryEdit, SIGNAL(textEdited(QString)),
        this, SLOT(rightBoundaryEdited(QString)));
    connect(mUi.boundaryEpsilonEdit, SIGNAL(textEdited(QString)),
        this, SLOT(boundaryEpsilonEdited(QString)));

    connect(mUi.startRunButton, SIGNAL(clicked()),
        this, SLOT(startRunClicked()));
    connect(mUi.startRunButton, SIGNAL(clicked()),
        &mModel, SLOT(startRun()));
    connect(&mModel, SIGNAL(runInfoChanged(int)),
        this, SLOT(updateData(int)));
    connect(&mModel, SIGNAL(runFinished()),
        this, SLOT(runFinishUpdate()));

    connect(mUi.showResultButton, SIGNAL(clicked()),
            mResultsDialog.get(), SLOT(show()));
}

void ODESMainWindow::initializePlots() {
    mUi.testProblemPlot->addGraph();
    mUi.testProblemPlot->addGraph();
    mUi.testProblemPlot->graph(0)->setPen(QPen(Qt::red));
    mUi.testProblemPlot->graph(0)->setName("Точное решение");
    mUi.testProblemPlot->graph(1)->setPen(QPen(Qt::darkGreen));
    mUi.testProblemPlot->graph(1)->setName("Численное решение");
    mUi.testProblemPlot->xAxis->setLabel("x");
    mUi.testProblemPlot->yAxis->setLabel("u, v");
    mUi.testProblemPlot->legend->setVisible(true);

    mUi.mainProblem1Plot->addGraph();
    mUi.mainProblem1Plot->graph(0)->setPen(QPen(Qt::darkGreen));
    mUi.mainProblem1Plot->xAxis->setLabel("x");
    mUi.mainProblem1Plot->yAxis->setLabel("v");

    mUi.mainProblem2UPlot->addGraph();
    mUi.mainProblem2UPlot->graph(0)->setPen(QPen(Qt::darkGreen));
    mUi.mainProblem2UPlot->xAxis->setLabel("x");
    mUi.mainProblem2UPlot->yAxis->setLabel("v");

    mUi.mainProblem2UPrimePlot->addGraph();
    mUi.mainProblem2UPrimePlot->graph(0)->setPen(QPen(Qt::darkGreen));
    mUi.mainProblem2UPrimePlot->xAxis->setLabel("x");
    mUi.mainProblem2UPrimePlot->yAxis->setLabel("v'");

    mPhasePlaneCurve = new QCPCurve(mUi.mainProblem2PhasePlanePlot->xAxis,
        mUi.mainProblem2PhasePlanePlot->yAxis);
    mUi.mainProblem2PhasePlanePlot->addPlottable(mPhasePlaneCurve);
    mPhasePlaneCurve->setPen(QPen(Qt::blue));
    mUi.mainProblem2PhasePlanePlot->xAxis->setLabel("v");
    mUi.mainProblem2PhasePlanePlot->yAxis->setLabel("v'");
}

void ODESMainWindow::clearTestProblemPlot() {
    mUi.testProblemPlot->graph(0)->clearData();
    mUi.testProblemPlot->graph(1)->clearData();
    mUi.testProblemPlot->xAxis->setRange(0.0, 1.0);
    mUi.testProblemPlot->yAxis->setRange(0.0, 1.0);
}

void ODESMainWindow::clearMainProblem1Plot() {
    mUi.mainProblem1Plot->graph(0)->clearData();
    mUi.mainProblem1Plot->xAxis->setRange(0.0, 1.0);
    mUi.mainProblem1Plot->yAxis->setRange(0.0, 1.0);
}

void ODESMainWindow::clearMainProblem2Plots() {
    mUi.mainProblem2UPlot->graph(0)->clearData();
    mUi.mainProblem2UPlot->xAxis->setRange(0.0, 1.0);
    mUi.mainProblem2UPlot->yAxis->setRange(0.0, 1.0);

    mUi.mainProblem2UPrimePlot->graph(0)->clearData();
    mUi.mainProblem2UPrimePlot->xAxis->setRange(0.0, 1.0);
    mUi.mainProblem2UPrimePlot->yAxis->setRange(0.0, 1.0);

    mPhasePlaneCurve->clearData();
    mUi.mainProblem2PhasePlanePlot->xAxis->setRange(0.0, 1.0);
    mUi.mainProblem2PhasePlanePlot->yAxis->setRange(0.0, 1.0);
}

void ODESMainWindow::updateEditsContents() {
    mUi.aEdit->setText(
        QString("%1").arg(mModel.a())
    );
    mUi.bEdit->setText(
        QString("%1").arg(mModel.b())
    );
    mUi.x0Edit->setText(
        QString("%1").arg(mModel.xInitial())
    );
    mUi.u0Edit->setText(
        QString("%1").arg(mModel.uInitial())
    );
    mUi.uPrime0Edit->setText(
        QString("%1").arg(mModel.uPrimeInitial())
    );
    mUi.h0Edit->setText(
        QString("%1").arg(mModel.initialStep())
    );
    mUi.LEEpsilonEdit->setText(
        QString("%1").arg(mModel.leControlEpsilon())
    );
    mUi.lowerStepLimitEdit->setText(
        QString("%1").arg(mModel.lowerStepLimit())
    );
    mUi.upperStepLimitEdit->setText(
        QString("%1").arg(mModel.upperStepLimit())
    );
    mUi.nMaxEdit->setText(
        QString("%1").arg(mModel.nMax())
    );
    mUi.rightBoundaryEdit->setText(
        QString("%1").arg(mModel.rightBoundary())
    );
    mUi.boundaryEpsilonEdit->setText(
        QString("%1").arg(mModel.boundaryEpsilon())
    );

    bool abNeeded = mModel.problemType() == ProblemType::MAIN_PROBLEM2;
    mUi.aLabel->setVisible(abNeeded);
    mUi.aEdit->setVisible(abNeeded);
    mUi.bLabel->setVisible(abNeeded);
    mUi.bEdit->setVisible(abNeeded);
    mUi.uPrime0Label->setVisible(abNeeded);
    mUi.uPrime0Edit->setVisible(abNeeded);
}

void ODESMainWindow::updateTestProblemPlot(int latestIndex) {
    typedef IterationInfo<1> IterInfo;
    QVector<double> us;
    QVector<double> vs;
    QVector<double> xs;

    for (int i = mPreviouslyLastIterationIndex + 1; i <= latestIndex; ++i) {
        const IterInfo& info = mModel.testProblemIntegrator()->iterationInfo(i);
        xs.push_back(info.point().x());
        us.push_back(mModel.testProblemAnalyticSolution(info.point().x()));
        vs.push_back(info.point().u(0));
    }

    mUi.testProblemPlot->graph(0)->addData(xs, us);
    mUi.testProblemPlot->rescaleAxes();
    mUi.testProblemPlot->replot();

    mUi.testProblemPlot->graph(1)->addData(xs, vs);
    mUi.testProblemPlot->rescaleAxes();
    mUi.testProblemPlot->replot();
}

void ODESMainWindow::updateMainProblem1Plot(int latestIndex) {
    typedef IterationInfo<1> IterInfo;
    QVector<double> vs;
    QVector<double> xs;

    for (int i = mPreviouslyLastIterationIndex + 1; i <= latestIndex; ++i) {
        const IterInfo& info =
            mModel.mainProblem1Integrator()->iterationInfo(i);
        xs.push_back(info.point().x());
        vs.push_back(info.point().u(0));
    }

    mUi.mainProblem1Plot->graph(0)->addData(xs, vs);
    mUi.mainProblem1Plot->rescaleAxes();
    mUi.mainProblem1Plot->replot();
}

void ODESMainWindow::updateMainProblem2Plots(int latestIndex) {
    typedef IterationInfo<2> IterInfo;
    QVector<double> v1s;
    QVector<double> v2s;
    QVector<double> xs;

    for (int i = mPreviouslyLastIterationIndex + 1; i <= latestIndex; ++i) {
        const IterInfo& info =
            mModel.mainProblem2Integrator()->iterationInfo(i);
        xs.push_back(info.point().x());
        v1s.push_back(info.point().u(0));
        v2s.push_back(info.point().u(1));
    }

    mUi.mainProblem2UPlot->graph(0)->addData(xs, v1s);
    mUi.mainProblem2UPlot->rescaleAxes();
    mUi.mainProblem2UPlot->replot();

    mUi.mainProblem2UPrimePlot->graph(0)->addData(xs, v2s);
    mUi.mainProblem2UPrimePlot->rescaleAxes();
    mUi.mainProblem2UPrimePlot->replot();

    mPhasePlaneCurve->addData(xs, v1s, v2s);
    mPhasePlaneCurve->rescaleAxes();
    mUi.mainProblem2PhasePlanePlot->replot();
}

void ODESMainWindow::updateTestProblemTable(int /* latestIndex */) {
    QItemSelectionModel* oldModel = mUi.testProblemTable->selectionModel();
    mUi.testProblemTable->setModel(new ODESTestProblemTableModel(mModel));
    delete oldModel;
}

void ODESMainWindow::updateMainProblem1Table(int /* latestIndex */) {
    QItemSelectionModel* oldModel = mUi.mainProblem1Table->selectionModel();
    mUi.mainProblem1Table->setModel(new ODESMainProblem1TableModel(mModel));
    delete oldModel;
}

void ODESMainWindow::updateMainProblem2Table(int /* latestIndex */) {
    QItemSelectionModel* oldModel = mUi.mainProblem2Table->selectionModel();
    mUi.mainProblem2Table->setModel(new ODESMainProblem2TableModel(mModel));
    delete oldModel;
}
