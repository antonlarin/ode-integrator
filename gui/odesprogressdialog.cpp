#include "guiutilities.hpp"
#include "odesprogressdialog.hpp"

ODESProgressDialog::ODESProgressDialog(ODESModel& model, QWidget *parent) :
    QDialog(parent), mModel(model)
{
    mUi.setupUi(this);
    connectSlots();
}

void ODESProgressDialog::initialize() {
    mUi.iterationIndexLabel->setText(QString("№ шага:"));
    mUi.currentXLabel->setText(indexTemplate.arg("x", "i", ":"));
    mUi.currentStepLabel->setText(indexTemplate.arg("h", "i", ":"));
    mUi.currentVLabel->setText(indexTemplate.arg("v", "i", ":"));
    mUi.currentVLEEstimateLabel->setText(QString("ОЛП:"));

    mUi.totalIterationCountLabel->setText(
        indexTemplate.arg("N", "max", ": %1").arg(mModel.nMax())
    );

    mUi.iterationProgressBar->setRange(0, mModel.nMax());
    mUi.iterationProgressBar->setValue(0);
    mUi.leftToBoundaryLabel->setText(
        indexTemplate.arg("b - x", "i", ":")
    );
}

void ODESProgressDialog::show() {
    initialize();
    QDialog::show();
}

void ODESProgressDialog::update(int latestIndex) {
    double x;
    double step;
    double v;
    double leEstimate;

    switch (mModel.problemType()) {
    case ProblemType::TEST_PROBLEM:
    {
        const IterationInfo<1>& latestIteration =
            mModel.testProblemIntegrator()->iterationInfo(latestIndex);
        x = latestIteration.point().x();
        step = latestIteration.step();
        v = latestIteration.point().u(0);
        leEstimate = latestIteration.leEstimate(0);
        break;
    }
    case ProblemType::MAIN_PROBLEM1:
    {
        const IterationInfo<1>& latestIteration =
            mModel.mainProblem1Integrator()->iterationInfo(latestIndex);
        x = latestIteration.point().x();
        step = latestIteration.step();
        v = latestIteration.point().u(0);
        leEstimate = latestIteration.leEstimate(0);
        break;
    }
    case ProblemType::MAIN_PROBLEM2:
    {
        const IterationInfo<2>& latestIteration =
            mModel.mainProblem2Integrator()->iterationInfo(latestIndex);
        x = latestIteration.point().x();
        step = latestIteration.step();
        v = latestIteration.point().u(1);
        leEstimate = latestIteration.leEstimate(1);
        break;
    }
    }


    mUi.iterationIndexLabel->setText(QString("№ шага: %1").arg(latestIndex));
    mUi.currentXLabel->setText(indexTemplate.arg("x", "i", ": %1").arg(x));
    mUi.currentStepLabel->setText(
        indexTemplate.arg("h", "i", ": %1").arg(step));
    mUi.currentVLabel->setText(indexTemplate.arg("v", "i", ": %1").arg(v));
    mUi.currentVLEEstimateLabel->setText(QString("ОЛП: %1").arg(leEstimate));

    mUi.iterationProgressBar->setValue(latestIndex);
    mUi.leftToBoundaryLabel->setText(
        indexTemplate.arg("b - x", "i", ": %1").
        arg(mModel.rightBoundary() - x));
}

void ODESProgressDialog::pauseRunRequested()
{
    mModel.pauseRun();
}

void ODESProgressDialog::connectSlots() {
    connect(&mModel, SIGNAL(runInfoChanged(int)),
        this, SLOT(update(int)));
    connect(mUi.pauseButton, SIGNAL(clicked()),
        this, SLOT(pauseRunRequested()));
    connect(&mModel, SIGNAL(runFinished()),
        this, SLOT(close()));
}
