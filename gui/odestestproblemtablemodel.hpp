#ifndef ODESTESTPROBLEMTABLEMODEL_HPP
#define ODESTESTPROBLEMTABLEMODEL_HPP

#include <QAbstractTableModel>
#include "odesmodel.hpp"

class ODESTestProblemTableModel : public QAbstractTableModel
{
public:
    ODESTestProblemTableModel(ODESModel& model);
    virtual ~ODESTestProblemTableModel();

    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant headerData(int section,
        Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data(const QModelIndex & index,
        int role = Qt::DisplayRole) const;

private:
    ODESModel& mModel;
};

#endif // ODESTESTPROBLEMTABLEMODEL_HPP
