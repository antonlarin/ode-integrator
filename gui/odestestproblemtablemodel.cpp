#include "odestestproblemtablemodel.hpp"

ODESTestProblemTableModel::ODESTestProblemTableModel(ODESModel& model) :
    mModel(model)
{}

ODESTestProblemTableModel::~ODESTestProblemTableModel() {}

int ODESTestProblemTableModel::rowCount(const QModelIndex& /* parent */) const {
    if (mModel.testProblemIntegrator() == nullptr) {
        return 0;
    } else {
        return mModel.testProblemIntegrator()->iterationsCount();
    }
}

int ODESTestProblemTableModel::columnCount(
    const QModelIndex& /* parent */) const {
    return 10;
}

QVariant ODESTestProblemTableModel::headerData(int section,
    Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    if (orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return "x_i";
        case 1:
            return "h_i-1";
        case 2:
            return "v_i";
        case 3:
            return "v2_i";
        case 4:
            return "v_i - v2_i";
        case 5:
            return "ОЛП";
        case 6:
            return "Дел. шага";
        case 7:
            return "Удв. шага";
        case 8:
            return "u_i";
        case 9:
            return "|u_i - v_i|";
        default:
            return QVariant();
        }
    } else {
        return section;
    }
}

QVariant ODESTestProblemTableModel::data(const QModelIndex& index,
    int role) const
{
    if (role != Qt::DisplayRole || mModel.testProblemIntegrator() == nullptr) {
        return QVariant();
    }

    const IterationInfo<1>& iterInfo =
        mModel.testProblemIntegrator()->iterationInfo(index.row());
    double u = mModel.testProblemAnalyticSolution(iterInfo.point().x());

    switch (index.column()) {
    case 0:
        return iterInfo.point().x();
    case 1:
        return iterInfo.step();
    case 2:
        return iterInfo.point().u(0);
    case 3:
        return (index.row() == 0 ||
            mModel.leControlType() == LEControlType::NoControl) ?
            QVariant("-") : QVariant(iterInfo.halfDoubleStepValue(0));
    case 4:
        return (index.row() == 0 ||
            mModel.leControlType() == LEControlType::NoControl) ?
            QVariant("-") : QVariant(iterInfo.sValue(0));
    case 5:
        return (index.row() == 0 ||
            mModel.leControlType() == LEControlType::NoControl) ?
            QVariant("-") : QVariant(iterInfo.leEstimate(0));
    case 6:
        return (index.row() == 0) ?
            QVariant("-") : QVariant(iterInfo.stepDecreaseCount());
    case 7:
        return (index.row() == 0) ?
            QVariant("-") : QVariant(iterInfo.stepIncreaseCount());
    case 8:
        return u;
    case 9:
        return fabs(u - iterInfo.point().u(0));
    default:
        return QVariant("-");
    }
}

