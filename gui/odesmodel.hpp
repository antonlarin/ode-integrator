#ifndef ODESMODEL_HPP
#define ODESMODEL_HPP

#include <memory>

#include <QObject>
#include <QThread>

#include "observer.hpp"
#include "ode/odeintegrator.hpp"
#include "ode/concreterhsfunctions.hpp"

enum class ProblemType {
    TEST_PROBLEM,
    MAIN_PROBLEM1,
    MAIN_PROBLEM2
};

class ODESModel : public QObject, public Observable
{
    Q_OBJECT

public:
    ODESModel(QObject* parent = 0);
    virtual ~ODESModel();

    ProblemType problemType() const;
    void setProblemType(ProblemType problemType);

    double a() const;
    void setA(double a);

    double b() const;
    void setB(double b);

    double xInitial() const;
    void setXInitial(double xInitial);

    double uInitial() const;
    void setUInitial(double uInitial);

    double uPrimeInitial() const;
    void setUPrimeInitial(double uPrimeInitial);

    double initialStep() const;
    void setInitialStep(double initialStep);

    LEControlType leControlType() const;
    void setLeControlType(LEControlType leControlType);

    double leControlEpsilon() const;
    void setLeControlEpsilon(double leControlEpsilon);

    double lowerStepLimit() const;
    void setLowerStepLimit(double lowerStepLimit);

    double upperStepLimit() const;
    void setUpperStepLimit(double upperStepLimit);

    int nMax() const;
    void setNMax(int nMax);

    double rightBoundary() const;
    void setRightBoundary(double rightBoundary);

    double boundaryEpsilon() const;
    void setBoundaryEpsilon(double boundaryEpsilon);

    bool running() const;

    double testProblemAnalyticSolution(double x) const;
    double maxDifferenceWithAnalyticSolution() const;
    double maxDifferenceWithAnalyticSolutionX() const;

    typedef ODEIntegrator<1, 4> ScalarIntegrator;
    ScalarIntegrator* testProblemIntegrator();
    ScalarIntegrator* mainProblem1Integrator();

    typedef ODEIntegrator<2, 4> SystemIntegrator;
    SystemIntegrator* mainProblem2Integrator();

signals:
    void runFinished();
    void runInfoChanged(int index);

public slots:
    void startRun();
    void pauseRun();

private:
    void startTestProblemRun();
    void startMainProblem1Run();
    void startMainProblem2Run();

    QThread mRunner;

    std::unique_ptr<ScalarIntegrator> mTestProblemIntegrator;
    std::unique_ptr<ScalarIntegrator> mMainProblem1Integrator;
    std::unique_ptr<SystemIntegrator> mMainProblem2Integrator;
    std::unique_ptr<IntegrationMethod<1, 4>> mScalarMethod;
    std::unique_ptr<IntegrationMethod<2, 4>> mSystemMethod;
    TestProblemRHSFunction mTestProblemRHSFunction;
    MainProblem1RHSFunction mMainProblem1RHSFunction;
    MainProblem2RHSFunction1 mMainProblem2RHSFunction1;
    std::unique_ptr<MainProblem2RHSFunction2> mMainProblem2RHSFunction2;

    ProblemType mProblemType;
    double mA;
    double mB;
    double mXInitial;
    double mUInitial;
    double mUPrimeInitial;
    double mInitialStep;
    LEControlType mLeControlType;
    double mLeControlEpsilon;
    double mLowerStepLimit;
    double mUpperStepLimit;
    int mNMax;
    double mRightBoundary;
    double mBoundaryEpsilon;

    double mMaxDifferenceWithAnalyticSolution;
    double mMaxDifferenceWithAnalyticSolutionX;

    bool mRunning;
};

#endif // ODESMODEL_HPP
