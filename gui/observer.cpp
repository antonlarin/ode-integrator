#include "observer.hpp"

void Observable::registerObserver(Observer* observer) {
    observers.push_back(observer);
}

void Observable::notify() {
    for (Observer* observer : observers) {
        observer->update();
    }
}
