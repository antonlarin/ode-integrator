#include "odesmainproblem2tablemodel.hpp"

ODESMainProblem2TableModel::ODESMainProblem2TableModel(ODESModel& model) :
    mModel(model)
{}

ODESMainProblem2TableModel::~ODESMainProblem2TableModel() {}

int ODESMainProblem2TableModel::rowCount(const QModelIndex& /* parent */)
    const {
    if (mModel.mainProblem2Integrator() == nullptr) {
        return 0;
    } else {
        return mModel.mainProblem2Integrator()->iterationsCount();
    }
}

int ODESMainProblem2TableModel::columnCount(
    const QModelIndex& /* parent */) const {
    return 8;
}

QVariant ODESMainProblem2TableModel::headerData(int section,
    Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    if (orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return "x_i";
        case 1:
            return "h_i-1";
        case 2:
            return "v_i";
        case 3:
            return "v2_i";
        case 4:
            return "v_i - v2_i";
        case 5:
            return "ОЛП";
        case 6:
            return "Дел. шага";
        case 7:
            return "Удв. шага";
        default:
            return QVariant();
        }
    } else {
        return section;
    }
}

QVariant ODESMainProblem2TableModel::data(const QModelIndex& index,
    int role) const
{
    if (role != Qt::DisplayRole || mModel.mainProblem2Integrator() == nullptr) {
        return QVariant();
    }

    const IterationInfo<2>& iterInfo =
        mModel.mainProblem2Integrator()->iterationInfo(index.row());

    switch (index.column()) {
    case 0:
        return iterInfo.point().x();
    case 1:
        return iterInfo.step();
    case 2:
        return iterInfo.point().u(1);
    case 3:
        return (index.row() == 0 ||
            mModel.leControlType() == LEControlType::NoControl) ?
            QVariant("-") : QVariant(iterInfo.halfDoubleStepValue(1));
    case 4:
        return (index.row() == 0 ||
            mModel.leControlType() == LEControlType::NoControl) ?
            QVariant("-") : QVariant(iterInfo.sValue(1));
    case 5:
        return (index.row() == 0 ||
            mModel.leControlType() == LEControlType::NoControl) ?
            QVariant("-") : QVariant(iterInfo.leEstimate(1));
    case 6:
        return (index.row() == 0) ?
            QVariant("-") : QVariant(iterInfo.stepDecreaseCount());
    case 7:
        return (index.row() == 0) ?
            QVariant("-") : QVariant(iterInfo.stepIncreaseCount());
    default:
        return QVariant("-");
    }
}
