#ifndef ODESMAINPROBLEM1TABLEMODEL_HPP
#define ODESMAINPROBLEM1TABLEMODEL_HPP

#include <QAbstractTableModel>
#include "odesmodel.hpp"

class ODESMainProblem1TableModel : public QAbstractTableModel
{
public:
    ODESMainProblem1TableModel(ODESModel& model);
    virtual ~ODESMainProblem1TableModel();

    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant headerData(int section,
        Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data(const QModelIndex & index,
        int role = Qt::DisplayRole) const;

private:
    ODESModel& mModel;
};

#endif // ODESMAINPROBLEM1TABLEMODEL_HPP
