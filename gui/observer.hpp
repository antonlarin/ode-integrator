#ifndef OBSERVER_HPP
#define OBSERVER_HPP

#include <vector>

using std::vector;

class Observer;

class Observable {
public:
    virtual ~Observable() {}

    void registerObserver(Observer* observer);
    void notify();

private:
    vector<Observer*> observers;
};


class Observer {
public:
    virtual ~Observer() {}

    virtual void update() = 0;
};

#endif // OBSERVER_HPP

